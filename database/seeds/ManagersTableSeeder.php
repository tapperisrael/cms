<?php

use Illuminate\Database\Seeder;

class ManagersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $manager = new \App\Models\Manager();
        $manager->email = 'boss@test.com';
        $manager->password = bcrypt(env('DEFAULT_PASSWORD'));
        $manager->company()->associate(\App\Models\Company::find(1));
        $manager->owner = 1;
        $manager->api_token = 123;
        $manager->save();

        $employee = new \App\Models\Manager();
        $employee->name = 'Employee';
        $employee->email = 'employee@test.com';
        $employee->password = bcrypt(env('DEFAULT_PASSWORD'));
        $employee->company()->associate(\App\Models\Company::find(1));
        $employee->owner = 0;
        $employee->save();
    }
}
