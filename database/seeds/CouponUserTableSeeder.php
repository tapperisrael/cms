<?php

use Illuminate\Database\Seeder;

class CouponUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $coupon = \App\Models\Coupon::find(5);
        $user = \App\Models\User::find(1);

        $user->coupons()->attach($coupon, ['expiration_date' => '2017-12-12', 'used' => 0]);

        $coupon->quantity -= 1;
        $coupon->save();
    }
}
