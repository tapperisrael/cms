<?php

use Illuminate\Database\Seeder;

class CouponsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $desc = '<div dir="rtl"><strong>תנאי שימוש</strong></div>
<ul dir="rtl">
<li>
<div><strong>בתוקף עד: 30.10.2017</strong></div>
</li>
<li>
<div>רכישת ומימוש גרופונים ללא הגבלה, בכפוף למלאי הזמין<br />כל סועד מחויב בשובר</div>
</li>
<li>
<div><strong>ניתן לממש את השובר בימים ובשעות:</strong><br />ראשון-שבת&nbsp;12:00-23:00</div>
</li>
<li>
<div><strong>בתאום מראש בלבד בטלפון:</strong>&nbsp;03-6768661<br />לא תקף ב-T.A ומשלוחים ולא ניתן לארוז / לקחת הביתה חלק או את כל מרכיבי הארוחה<br />לא כולל עסקיות,&nbsp;כפל מבצעים והנחות<br />יש להציג את השובר למלצר בתחילת הארוחה<br />לא ניתן לממש את השובר יחד עם הטבת יום הולדת או הנחת חבר מועדון<br />במסגרת הארוחה בית העסק יאפשר להזמין ללא הגבלה מתוך מגוון הבשרים המצויינים לעיל והמצויים במלאי</div>
</li>
<li>
<div><strong>מדיניות ביטולים</strong><strong>:</strong>&nbsp;בכפוף&nbsp;<a href="https://www.grouponisrael.co.il/%D7%AA%D7%A0%D7%90%D7%99_%D7%A9%D7%99%D7%9E%D7%95%D7%A9" target="_blank">לתנאי השימוש</a>&nbsp;של גרופון (סעיף 24) ולהוראות חוק הגנת הצרכן</div>
</li>
<li>
<div>הגרופון יהיה זמין בתום הרכישה תחת \'ההזמנות שלי\'</div>
</li>
<li>
<div>ראו את&nbsp;<a href="https://www.grouponisrael.co.il/%D7%90%D7%95%D7%AA%D7%99%D7%95%D7%AA_%D7%A7%D7%98%D7%A0%D7%95%D7%AA_%D7%90%D7%95%D7%A0%D7%99%D7%91%D7%A8%D7%A1%D7%9C%D7%99%D7%95%D7%AA" target="_blank">הכללים</a>&nbsp;החלים על כל העסקאות</div>
</li>
<li>
<div>בית העסק (ולא גרופון) הנו האחראי הבלעדי לאיכות, לטיב ולטיפול במוצרים ו/או בשירותים שפורסמו.</div>
</li>
</ul>';
        
        $coupon1 = new \App\Coupon();
        $coupon1->title = 'ארוחת בוקר חינם';
        $coupon1->price = 10;
        $coupon1->description = $desc;
        $coupon1->company()->associate(\App\Company::find(1));
        $coupon1->quantity = 10;
        $coupon1->save();

        $coupon11 = new \App\Coupon();
        $coupon11->title = 'קפה ומאפה חינם';
        $coupon11->price = 10;
        $coupon11->description = $desc;
        $coupon11->company()->associate(\App\Company::find(1));
        $coupon11->quantity = 10;
        $coupon11->save();

        $coupon2 = new \App\Coupon();
        $coupon2->title = 'ארוחה זוגית 1+1';
        $coupon2->price = 20;
        $coupon2->description = $desc;
        $coupon2->company()->associate(\App\Company::find(2));
        $coupon2->quantity = 20;
        $coupon2->save();

        $coupon22 = new \App\Coupon();
        $coupon22->title = 'סנדויץ טונה חינם';
        $coupon22->price = 20;
        $coupon22->description = $desc;
        $coupon22->company()->associate(\App\Company::find(2));
        $coupon22->quantity = 20;
        $coupon22->save();

        $coupon3 = new \App\Coupon();
        $coupon3->title = 'צ`יסר ויסקי בבר';
        $coupon3->price = 30;
        $coupon3->description = $desc;
        $coupon3->company()->associate(\App\Company::find(3));
        $coupon3->quantity = 30;
        $coupon3->save();

        $coupon33 = new \App\Coupon();
        $coupon33->title = 'ספא 1+1';
        $coupon33->price = 30;
        $coupon33->description = $desc;
        $coupon33->company()->associate(\App\Company::find(3));
        $coupon33->quantity = 30;
        $coupon33->save();
        
        $coupon4 = new \App\Coupon();
        $coupon4->title = 'פיצה אישית חינם';
        $coupon4->price = 40;
        $coupon4->description = $desc;
        $coupon4->company()->associate(\App\Company::find(4));
        $coupon4->quantity = 40;
        $coupon4->save();

        $coupon44 = new \App\Coupon();
        $coupon44->title = 'פיצה משפחתית 50% הנחה';
        $coupon44->price = 40;
        $coupon44->description = $desc;
        $coupon44->company()->associate(\App\Company::find(4));
        $coupon44->quantity = 40;
        $coupon44->save();

        $coupon5 = new \App\Coupon();
        $coupon5->title = 'ניגירי סלמון חינם';
        $coupon5->price = 50;
        $coupon5->description = $desc;
        $coupon5->company()->associate(\App\Company::find(5));
        $coupon5->quantity = 50;
        $coupon5->save();

        $coupon55 = new \App\Coupon();
        $coupon55->title = '50% הנחה אחרי 22.00';
        $coupon55->price = 50;
        $coupon55->description = $desc;
        $coupon55->company()->associate(\App\Company::find(5));
        $coupon55->quantity = 50;
        $coupon55->save();

        $coupon6 = new \App\Coupon();
        $coupon6->title = 'כניסה חינם לילדים עד גיל 14';
        $coupon6->price = 60;
        $coupon6->description = $desc;
        $coupon6->company()->associate(\App\Company::find(6));
        $coupon6->quantity = 60;
        $coupon6->save();

        $coupon66 = new \App\Coupon();
        $coupon66->title = 'כרטיס 1+1';
        $coupon66->price = 60;
        $coupon66->description = $desc;
        $coupon66->company()->associate(\App\Company::find(6));
        $coupon66->quantity = 60;
        $coupon66->save();

        $coupon7 = new \App\Coupon();
        $coupon7->title = 'כוס בירה חינם';
        $coupon7->price = 70;
        $coupon7->description = $desc;
        $coupon7->company()->associate(\App\Company::find(7));
        $coupon7->quantity = 70;
        $coupon7->save();

        $coupon77 = new \App\Coupon();
        $coupon77->title = 'צ`סיר ויסקי חינם';
        $coupon77->price = 70;
        $coupon77->description = $desc;
        $coupon77->company()->associate(\App\Company::find(7));
        $coupon77->quantity = 70;
        $coupon77->save();

        $coupon8 = new \App\Coupon();
        $coupon8->title = '50% הנחה על הסנדויצ`ים';
        $coupon8->price = 80;
        $coupon8->description = $desc;
        $coupon8->company()->associate(\App\Company::find(8));
        $coupon8->quantity = 80;
        $coupon8->save();

        $coupon88 = new \App\Coupon();
        $coupon88->title = 'ארוחה 1+1';
        $coupon88->price = 80;
        $coupon88->description = $desc;
        $coupon88->company()->associate(\App\Company::find(8));
        $coupon88->quantity = 80;
        $coupon88->save();

        $coupon9 = new \App\Coupon();
        $coupon9->title = 'ארוחת בוקר חינם';
        $coupon9->price = 90;
        $coupon9->description = $desc;
        $coupon9->company()->associate(\App\Company::find(9));
        $coupon9->quantity = 90;
        $coupon9->save();

        $coupon99 = new \App\Coupon();
        $coupon99->title = 'יום ספא 50% הנחה';
        $coupon99->price = 90;
        $coupon99->description = $desc;
        $coupon99->company()->associate(\App\Company::find(9));
        $coupon99->quantity = 90;
        $coupon99->save();

        $coupon10 = new \App\Coupon();
        $coupon10->title = 'עיצוב לאפליקציה 50% הנחה';
        $coupon10->price = 100;
        $coupon10->description = $desc;
        $coupon10->company()->associate(\App\Company::find(10));
        $coupon10->quantity = 100;
        $coupon10->save();

        $coupon100 = new \App\Coupon();
        $coupon100->title = 'עוד קופון אין לי כוח כבר';
        $coupon100->price = 100;
        $coupon100->description = $desc;
        $coupon100->company()->associate(\App\Company::find(10));
        $coupon100->quantity = 100;
        $coupon100->save();
    }
}
