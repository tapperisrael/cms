<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class CompanyCategory extends Model
{

    use Mediable;

    protected $hidden = ['deleted_at','created_at','updated_at'];
    protected $with = ['companies'];
    protected $appends = ['media1'];

  /*  public function company()
    {
        return $this->hasMany(Company::class,'region_id');
    }*/

    public function company_subcategory()
    {
        return $this->hasMany(CompanySubcategory::class);
    }


    public function companies()
    {
        return $this->hasManyThrough(Company::class, CompanySubcategory::class);
    }


    public function getMedia1Attribute()
    {
        $this->loadMedia();
        return $this->media[0]->getUrl();
    }

}
